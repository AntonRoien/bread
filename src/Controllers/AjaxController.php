<?php


namespace Bread\Controllers;


use Bread\Repositories\CheckRepository;
use Bread\Repositories\ViewCheckService;
use DateTime;
use DateTimeZone;
use Exception;

/**
 * Class AjaxController
 * @package Bread\Controllers
 * @TODO переделть вынести сервис.
 */
class AjaxController extends AbstractController
{
    /**
     * @param $array
     * @throws Exception
     */
    public function run($array)
    {
        if (key_exists('month', $array)) {
            echo $this->getStatsByMonth($array['month']);
        } else {
            echo $this->getAllStats();
        }
    }

    /**
     * @param $month
     * @return false|string
     * @throws Exception
     */
    protected function getStatsByMonth($month)
    {
        $service = new ViewCheckService(new CheckRepository());
        $start = new DateTime('2018-'.$month.'-01');
        $end = clone $start;
        $end->modify('last day of this month');

        return json_encode(
            [
                'byMonth' => true,
                'stats' =>
                    $service->getCheckSumByDates(
                        $start,
                        $end,
                        new DateTimeZone('Etc/GMT-3')
                    ),
            ]
        );
    }

    /**
     * @return false|string
     */
    protected function getAllStats()
    {
        $service = new ViewCheckService(new CheckRepository());

        return json_encode(
            [
                'byMonth' => false,
                'stats' => $service->getCheckSum(new DateTimeZone('Etc/GMT-3')),
            ]
        );
    }

}