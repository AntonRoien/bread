<?php


namespace Bread\Controllers;


abstract class AbstractController
{
    const TEMPLATE_DIR = __DIR__.'/../Templates/';

    abstract public function run($array);
}