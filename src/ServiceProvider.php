<?php


namespace Bread;

class ServiceProvider
{
    private static $instanse;

    private $listOfSeriveProvider = [];

    private function __construct()
    {
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (!static::$instanse instanceof ServiceProvider) {
             static::$instanse = new self();
        }

        return static::$instanse;
    }

    public function addService($name, $service)
    {
        $this->listOfSeriveProvider[$name] = $service;
    }

    private function checkIsInitialized($name)
    {
        if (array_key_exists($name, $this->listOfSeriveProvider)) {
            return true;
        } else {
            throw new \Exception('Object not initialized');
        }
    }

    public function getService($name)
    {
        $this->checkIsInitialized($name);
        return $this->listOfSeriveProvider[$name];
    }
}