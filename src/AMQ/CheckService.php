<?php


namespace Bread\AMQ;


use Bread\Models\Check;
use Bread\Repositories\CheckRepository;
use DateTime;
use DateTimeZone;
use Exception;
use MongoDB\BSON\UTCDateTime;

/**
 * Class CheckService
 * @package Bread\AMQ
 */
class CheckService
{

    /** @var CheckRepository */
    private $checkRepository;

    /**
     * CheckService constructor.
     * @param $checkRepository
     */
    public function __construct($checkRepository)
    {
        $this->checkRepository = $checkRepository;
    }

    /**
     * @param $item
     * @throws Exception
     */
    public function saveItem($item)
    {
        if ($this->validateItem($item)) {
            $this->checkRepository->saveOne($this->createCheckByMQItem($item));
        }
    }

    /**
     * @param $item
     * @return bool
     */
    public function validateItem($item)
    {
        if ($item->createDate && $item->sum && $item->number && $item->cashRegisterId) {
            return true;
        } else {
            false;
        }
    }

    /**
     * @param $item
     * @return Check
     * @throws Exception
     */
    public function createCheckByMQItem($item)
    {
        $date = new DateTime();
        $date->setTimestamp($item->createDate);
        $date->setTimezone(new DateTimeZone($item->timeZone));
        return (new Check())
            ->setSum($item->sum)
            ->setCashRegisterId($item->cashRegisterId)
            ->setNumber($item->number)
            ->setCreateData($date);
    }
}