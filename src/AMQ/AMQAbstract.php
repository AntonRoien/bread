<?php


namespace Bread\AMQ;


use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class AMQAbstract
{
    const QUEUE = 'MessagesQueue';

    protected $chanel;
    protected $connection;

    public function shutdown()
    {
        $this->chanel->close();
        $this->connection->close();
    }

    public function __construct()
    {

        register_shutdown_function([$this, 'shutdown']);

        $connection = new AMQPStreamConnection(
            getenv('RMQ_HOST'),
            getenv('RMQ_PORT'),
            getenv('RMQ_USER'),
            getenv('RMQ_PASS')
        );
        $this->chanel = $connection->channel();
        $this->connection = $connection;
        $this->initChanel();
    }

    abstract protected function initChanel();

}