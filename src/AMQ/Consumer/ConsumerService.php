<?php


namespace Bread\AMQ\Consumer;


use Bread\AMQ\AMQAbstract;
use Bread\AMQ\CheckService;
use Bread\Repositories\CheckRepository;
use ErrorException;
use Exception;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class ConsumerService
 * @package Bread\AMQ\Consumer
 */
class ConsumerService extends AMQAbstract
{
    /** @var CheckService */
    private $checkService;

    /**
     * @param AMQPMessage $message
     */
    public function getMessage($message)
    {
        try {
            $this->checkService->saveItem(json_decode($message->body));
        } catch (Exception $exception) {
            //TODO обработка ошибок
        }


        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        // Send a message with the string "quit" to cancel the consumer.
        if ($message->body === 'quit') {
            $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_tag']);
        }
    }

    /**
     * @throws ErrorException
     */
    public function run()
    {
        while (count($this->getChanel()->callbacks)) {
            $this->getChanel()->wait();
        }
    }

    /**
     * @return AMQPChannel
     */
    public function getChanel()
    {
        return $this->chanel;
    }

    /**
     *
     */
    protected function initChanel()
    {

        //TODO Вынести все в сервис провайдер.
        $this->checkService = new CheckService(new CheckRepository());

        $this->getChanel()->basic_consume(
            getenv('RMQ_BASE_QUEUE'),
            'base',
            false,
            false,
            false,
            false,
            [$this, 'getMessage']
        );
    }
}