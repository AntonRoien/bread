<?php


namespace Bread\AMQ\Publisher;


use Bread\AMQ\AMQAbstract;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class PublisherService
 * @package Bread\AMQ\Publisher
 */
class PublisherService extends AMQAbstract
{

    /**
     *
     */
    const MESSAGE_PROPERTIES = [
        'content_type' => 'text/plain',
        'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
    ];

    /**
     * @param array $data
     */
    public function sent($data)
    {
            $toSend = new AMQPMessage(
                $this->toJSON($data), static::MESSAGE_PROPERTIES
            );
            $this->chanel->basic_publish($toSend,getenv('RMQ_BASE_EXCHANGE'));
    }

    /**
     * @param $array
     * @return false|string
     */
    private function toJSON($array)
    {
        return json_encode($array);
    }

    /**
     * Инициализация роутинга и очереди
     */
    protected function initChanel()
    {
        $this->chanel->exchange_declare(getenv('RMQ_BASE_EXCHANGE'), 'fanout', false, false, false);
        $this->chanel->queue_declare(getenv('RMQ_BASE_QUEUE'), false, true, false, false);
        $this->chanel->queue_bind(getenv('RMQ_BASE_QUEUE'), getenv('RMQ_BASE_EXCHANGE'));
    }
}