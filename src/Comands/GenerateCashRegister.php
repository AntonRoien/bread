<?php

use Bread\Models\CashRegister;
use Bread\Repositories\CashRegisterRepository;

require __DIR__ . '/../../autoload.php';
$cashRegisterRepository = new CashRegisterRepository();

$cashRegisterArray = [
    (new CashRegister())->setId(1)->setTimeZone(new DateTimeZone('Etc/GMT-8')),
    (new CashRegister())->setId(2)->setTimeZone(new DateTimeZone('Etc/GMT+0')),
    (new CashRegister())->setId(3)->setTimeZone(new DateTimeZone('Etc/GMT+8')),
];

$cashRegisterRepository->save($cashRegisterArray);