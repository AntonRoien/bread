<?php

use Bread\Repositories\CashRegisterRepository;
use Bread\Seeders\Check as CheckGenerator;
use Bread\Seeders\Configs\Between;
use Bread\Seeders\Configs\Check;

require __DIR__ . '/../../autoload.php';
$cashRegisterRepository = new CashRegisterRepository();

$checkConfig = new Check();
//Сумма чека
$checkConfig->setSum(new Between(80, 150));
//Количество чеков в период
$checkConfig->setCountByDay(new Between(400,800));
$checkGenerator = new CheckGenerator(
    $cashRegisterRepository->getAll(),
    $checkConfig
);

//Стартовая дата генирации
$start = new DateTime('2018-01-01');
//Период, например P1D - каждый день
$interval = new DateInterval('P1D');
//Конечная дата генирации.
$end = new DateTime('2018-12-31');
$period = new DatePeriod($start, $interval, $end);
$checkGenerator->generate($period);

$publiser = new Bread\AMQ\Publisher\PublisherService();

/** @var \Bread\Models\Check $item */
foreach ($checkGenerator->generate($period) as $item) {
    // Получение чека и отправка в очередь
    $publiser->sent($item);
}
