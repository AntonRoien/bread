<?php
require __DIR__ . '/../../autoload.php';

$consumer = new \Bread\AMQ\Consumer\ConsumerService();
$consumer->run();
