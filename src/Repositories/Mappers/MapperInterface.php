<?php


namespace Bread\Repositories\Mappers;


interface MapperInterface
{
    public function mapping($item);
    public function validation($array);
}