<?php


namespace Bread\Repositories\Mappers;

use Bread\Models\CashRegister as Model;
use DateTimeZone;

class CashRegisterMapper implements MapperInterface
{
    /**
     * @param $item
     * @return Object
     */
    public function mapping($item)
    {
        return (new Model($item->_id))
            ->setTimeZone(new DateTimeZone($item->timeZone->timezone))
            ->setId($item->id);
    }

    /**
     * @param $item
     * @return bool
     * @throws MapperException
     */
    public function validation($item)
    {
        if (
            ($item->id != null) && ($item->timeZone->timezone)
        ) {
            return true;
        } else {
            throw new MapperException('Broken item _id '.$item->_id);
        }
    }
}