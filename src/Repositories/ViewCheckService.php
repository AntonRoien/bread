<?php


namespace Bread\Repositories;


use DateTime;
use DateTimeZone;
use MongoDB\BSON\UTCDateTime;

class ViewCheckService
{
    private $repository;

    /**
     * ViewCheckService constructor.
     * TODO вынеси репозиторий.
     * @param CheckRepository $checkRepository
     */
    public function __construct(CheckRepository $checkRepository)
    {
        $this->repository = $checkRepository;
    }

    public function getCheckSum(DateTimeZone $timeZone)
    {
        return $this->format(
            $this->repository->aggregate(
                [
                    [
                        '$group' => [
                            '_id' => [
                                'date' => [
                                    '$dateToString' => [
                                        'date' => ['$toDate' => '$createDate'],
                                        'timezone' => 'Etc/GMT-3',
                                        'format' => '%m.%Y',
                                    ],
                                ],
                                'cashRegisterId' => '$cashRegisterId',
                            ],
                            'sum' => ['$sum' => '$sum'],
                        ],
                    ],
                    [
                        '$sort' => ['createDate' => -1],
                    ],
                ]
            ),
            $timeZone
        );
    }

    private function format($array, DateTimeZone $timeZone)
    {

        $head = [];
        $body = [];
        foreach ($array as $item) {
            $head[$item['_id']['cashRegisterId']] = $item['_id']['cashRegisterId'];
            $body[$item['_id']['date']][$item['_id']['cashRegisterId']] = $item['sum'];
        }
        ksort($head);
        ksort($body);
        return ['head' => $head, 'body' => $body];
    }

    public function getCheckSumByDates(DateTime $start, DateTime $end, DateTimeZone $timeZone)
    {
        return $this->format(
            $this->repository->aggregate(
                [
                    [
                        '$match' => [
                            'createDate' => [
                                '$gte' => new UTCDateTime($start),
                                '$lt' => new UTCDateTime($end),
                            ],
                        ],
                    ],
                    [
                        '$group' => [
                            '_id' => [
                                'date' => [
                                    '$dateToString' => [
                                        'date' => ['$toDate' => '$createDate'],
                                        'timezone' => 'Etc/GMT-3',
                                        'format' => '%d.%m.%Y',
                                    ],
                                ],
                                'cashRegisterId' => '$cashRegisterId',
                            ],
                            'sum' => ['$sum' => '$sum'],
                        ],
                    ],
                    [
                        '$sort' => ['createDate' => -1],
                    ],
                ]
            ),
            $timeZone
        );
    }
}