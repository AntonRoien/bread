<?php


namespace Bread\Repositories;


use Bread\Repositories\Mappers\CheckMapper;
use MongoDB\Client;

/**
 * Class CheckRepository
 * @package Bread\Repositories
 */
class CheckRepository
{

    /** @var \MongoDB\Collection */
    protected $collection;
    /** @var CheckMapper */
    protected $mapper;

    /**
     * CashRegister constructor.
     */
    public function __construct()
    {
        $this->collection = (new Client('mongodb://mongo/'))->bread->check;
        $this->mapper = new CheckMapper();
    }


    /**
     * @param array $items
     */
    public function save(Array $items)
    {
        $this->collection->insertMany($items);
    }

    /**
     * @param $item
     */
    public function saveOne($item)
    {
        $this->collection->insertOne($item);
    }

    /**
     * @param $pipline
     * @return \Traversable
     */
    public function aggregate($pipline)
    {
        return $this->collection->aggregate($pipline);
    }

}
