<?php
//TODO обеденить в один репозиторий и для чеков и для касс так как методы одинаковые,
// и получать маперы и настройки извнвне, через срвис провайдер.


namespace Bread\Repositories;


use Bread\Repositories\Mappers\MapperException;
use MongoDB\Client;
use Bread\Repositories\Mappers\CashRegisterMapper as Mapper;

/**
 * Class CashRegister
 * @package Bread\Repositories
 */
class CashRegisterRepository
{

    /** @var \MongoDB\Collection */
    protected $collection;
    /** @var Mapper */
    protected $mapper;

    /**
     * CashRegister constructor.
     */
    public function __construct()
    {
        $this->collection = (new Client('mongodb://mongo/'))->bread->cash_register;
        $this->mapper = new Mapper();
    }

    /**
     * @return \Bread\Models\CashRegister[]
     * //TODO нужно добавить заглушку для поиска (не в рамках ТЗ)
     */
    public function getAll()
    {
        $cursor = $this->collection->find();
        $result = [];
        foreach ($cursor as $item) {
            try {
                $result[] = $this->mapper->validation($item) ? $this->mapper->mapping($item) : null;
            } catch (MapperException $exception) {
                //TODO To log with _uid
            }
        }
        return $result;
    }

    /**
     * @param array $items
     */
    public function save(Array $items)
    {
        $this->collection->insertMany($items);
    }

    /**
     * @param $item
     */
    public function saveOne($item)
    {
        $this->collection->insertOne($item);
    }

}