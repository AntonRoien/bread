<?php


namespace Bread\Models;

use DateTimeZone;

class CashRegister
{
    /** @var string */
    protected $_id;
    /** @var int */
    public $id;
    /** @var DateTimeZone */
    public $timeZone;

    /**
     * CashRegister constructor.
     * @param string $_id
     */
    public function __construct(string $_id = null)
    {
        $this->_id = $_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return CashRegister
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return DateTimeZone
     */
    public function getTimeZone(): DateTimeZone
    {
        return $this->timeZone;
    }

    /**
     * @param DateTimeZone $timeZone
     * @return CashRegister
     */
    public function setTimeZone(DateTimeZone $timeZone): CashRegister
    {
        $this->timeZone = $timeZone;

        return $this;
    }



}