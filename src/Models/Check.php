<?php


namespace Bread\Models;


use DateTime;
use MongoDB\BSON\UTCDateTime;

/**
 * Class Check
 * @package Bread\Models
 */
class Check
{
    /** @var string */
    public $uid;
    /** @var int */
    public $number;
    /** @var int */
    public $cashRegisterId;
    /** @var UTCDateTime */
    public $createDate;
    /** @var float */
    public $sum;

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param $uid
     * @return $this
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return int
     */
    public function getCashRegisterId()
    {
        return $this->cashRegisterId;
    }

    /**
     * @param $cashRegisterId
     * @return $this
     */
    public function setCashRegisterId($cashRegisterId)
    {
        $this->cashRegisterId = $cashRegisterId;

        return $this;
    }

    /**
     * @return DateTime
     * @throws \Exception
     */
    public function getCreateData(): DateTime
    {
        return $this->createDate->toDateTime();
    }

    /**
     * @param DateTime $createData
     * @return Check
     */
    public function setCreateData(DateTime $createData): Check
    {
        $this->createDate = new UTCDateTime($createData);
        return $this;
    }

    /**
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param $sum
     * @return $this
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

}