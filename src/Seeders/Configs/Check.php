<?php


namespace Bread\Seeders\Configs;

use DatePeriod;

class Between
{
    /** @var int */
    protected $min;
    /** @var int */
    protected $max;

    /**
     * Between constructor.
     * @param int $min
     * @param int $max
     */
    public function __construct(int $min, int $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * @return int
     */
    public function getMin(): int
    {
        return $this->min;
    }

    /**
     * @return int
     */
    public function getMax(): int
    {
        return $this->max;
    }


}

class Check
{
    /** @var Between */
    protected $sum;
    /** @var Between */
    protected $countByDay;
    /** @var Between */
    protected $workTime;
    /** @var DatePeriod */
    protected $dayForGenerate;

    /**
     * @return Between
     */
    public function getSum(): Between
    {
        return $this->sum;
    }

    /**
     * @param Between $sum
     * @return Check
     */
    public function setSum(Between $sum): Check
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @return Between
     */
    public function getCountByDay(): Between
    {
        return $this->countByDay;
    }

    /**
     * @param Between $countByDay
     * @return Check
     */
    public function setCountByDay(Between $countByDay): Check
    {
        $this->countByDay = $countByDay;

        return $this;
    }
}