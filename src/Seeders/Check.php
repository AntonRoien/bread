<?php


namespace Bread\Seeders;

use Bread\Models\CashRegister;
use DatePeriod;
use DateTime;
use Exception;
use Generator;

/**
 * Class Check
 * @package Bread\Seeders
 *
 * @TODO переделать генерацию номеров для чеков, сделать уникальным для каждого магазина
 * @TODO при генерации еще одной пачки учитывать последний чек в моноге
 */
class Check
{
    /** @var int - номер чека, для инкремейнта во время генерации */
    protected $number;
    /** @var CashRegister[] */
    protected $arrayOfCashRegister;
    /** @var Configs\Check */
    protected $config;

    /**
     * Check constructor.
     * @param CashRegister[] $arrayOfCashRegister
     * @param Configs\Check $config
     * @param int $number
     */
    public function __construct(array $arrayOfCashRegister, Configs\Check $config, $number = 0)
    {
        $this->number = $number;
        $this->arrayOfCashRegister = $arrayOfCashRegister;
        $this->config = $config;
    }

    /**
     * @param DatePeriod $datePeriod
     * @return Generator
     * @throws Exception
     */
    public function generate(DatePeriod $datePeriod)
    {
        foreach ($datePeriod as $date) {
            foreach ($this->arrayOfCashRegister as $item) {
                yield from $this->generateByDay($date, $item);
            }
        }
    }

    /**
     * @param DateTime $dateTime
     * @param CashRegister $cashRegister
     * @return Generator
     * @throws Exception
     */
    public function generateByDay(DateTime $dateTime, CashRegister $cashRegister)
    {
        $dateTime->setTimezone($cashRegister->getTimeZone());
        $count = rand($this->config->getCountByDay()->getMin(), $this->config->getCountByDay()->getMax());
        for ($i = 1; $i < $count; ++$i) {
            yield $this->generateItem(clone $dateTime, $cashRegister);
        }
    }


    /**
     * @param DateTime $date
     * @param CashRegister $cashRegister
     * @return array
     */
    public function generateItem(DateTime $date, CashRegister $cashRegister)
    {
        $date->setTime(rand(6,22), rand(0,60), rand(0,60));
        return [
            'cashRegisterId' => $cashRegister->getId(),
            'sum' => rand($this->config->getSum()->getMin(), $this->config->getSum()->getMax()),
            'number' => $this->number++,
            'createDate' => $date->getTimestamp(),
            'timeZone' => $cashRegister->getTimeZone()->getName(),
        ];

    }
}