<?php
require __DIR__ . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Bread\ServiceProvider;
use Dotenv\Dotenv;

$dotenv = Dotenv::create(__DIR__);
$dotenv->load();

//** load */
$serviceProvider = ServiceProvider::getInstance();
$serviceProvider->addService(Dotenv::class, $dotenv);
