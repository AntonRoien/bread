// A $( document ).ready() block.
$(document).ready(function () {
});

function getData(link) {
    $('#head').empty();
    $('#body').empty();
    $.ajax({
        url: link,
        dataType: 'json',
        success: function (data) {
            format(data);
        }
    });
}

function format(array) {
    makeHead(array.stats.head,array.byMonth);
    makeBody(array.stats.body, array.byMonth);
}

function makeHead(items,byMonth) {
    if (!byMonth) {
        $('#head').append('<th scope="col"></th>');
    }
    $('#head').append('<th scope="col">Дата</th>');

    for (var prop in items) {
        $('#head').append('<th scope="col">Касса: ' + items[prop] + '</th>');
    }
}

function makeBody(items, byMonth) {
    for (var item in items) {
        var dateElement = items[item];

        var str = '<tr>';
        if (!byMonth) {
            str = str + '<td><button onclick="getData(\''+'ajax/'+getMonthByString(item)+'\')">За месяц</button></td>';
        }
        str = str + '<th scope="row">'+item+'</a></th>';
        for (var element in dateElement) {
            //console.log(dateElement[element]);
            str = str+'<td cassa="'+element+'">'+dateElement[element]+'</td>';
        }
        str = str +'</tr>';
        $('#body').append(str);
    }
}

function getMonthByString(string) {
   return parseInt(string.substring(0., string.indexOf('.',0)), 10);
}
