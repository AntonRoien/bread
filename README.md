Для локальной разработки используется https://laradock.io

прописать хост:
127.0.0.1 bread.local

Для поднятия локального инстанса:

cd laradoc
docker-compose up -d nginx php-fpm rabbitmq mongo

Запустится докер контейнеры с нужным ПО

Для подключение по SSH к докеру
 docker-compose exec workspace bash
 
Сгенерировать кассы (в консоли докера из дериктории проекта):
    php src/Comands/GenerateCashRegister.php 
В файле можно добавить нужные количество касс помимо 3х основных

Сгенерировать чеки:
    php src/Comands/GenerateCheck.php
Также в этом файле можно указать следующее:
1) Период генерации.
2) Суммы чеков (min, max)
3) Количество чеков в конкретный период (min, max) 
Все сгенерированные чеки идут в очередь RMQ

Запустить коньсюмер для очереди чеков:
    php src/Comands/StartConsumeQueue.php 

